# Robotic Car Assembly

## Description

This is the Electrical and Mechanical Construction of the Robot body.

## Hardware Components

- [Raspberry Pi 4 Model B 8Gb Ram](https://www.amazon.eg/gp/product/B0899VXM8F/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1)

- [Micro SD Card Reader](https://www.amazon.eg/-/en/TF-Micro-SD-Card-Reader/dp/B091C9W4DQ/ref=sr_1_8?crid=1OP0X3V3O8MXL&keywords=micro+sd+card+reader&qid=1661800955&sprefix=micro+sd+card+reader%2Caps%2C310&sr=8-8)

- [Micro 32GB SD Card](https://www.amazon.eg/-/en/Kingston-32GB-MicroSD-Canvas-Adaptor/dp/B07YGZ7FY7/ref=sr_1_1?keywords=32+gb+sd+card&qid=1661801000&sprefix=32+%2Caps%2C164&sr=8-1)

- [2 x webcam cameras](https://www.amazon.eg/-/en/Creative-Labs-Digital-PC-Camera/dp/B091JTDXK5/ref=sr_1_17?crid=19W8ZOFYBR42A&keywords=webcam+camera&qid=1664565061&refinements=p_36%3A26313957031&rnid=22080723031&s=electronics&sprefix=webcam+camera%2Celectronics%2C128&sr=1-17)

- [4 Wheel Robot Chassis](https://www.amazon.eg/-/en/Arduino-Smart-Robot-Wheel-Chassis/dp/B07N6LYYNM/ref=sr_1_2?crid=1QB64X9QA5DYL&keywords=4+Wheel+Robot+Chassis&qid=1661801055&sprefix=4+wheel+robot+chassis%2Caps%2C144&sr=8-2)

- [Dual H-Bridge IC](https://www.amazon.eg/-/en/Pcchips-Bridge-Stepper-Motor-Controller/dp/B07NDYWN26/ref=sr_1_1?crid=39EP929V08R41&keywords=Dual+H-Bridge+IC&qid=1661801095&sprefix=dual+h-bridge+ic%2Caps%2C120&sr=8-1)

- [2 x 18650 Recharagable Battaries](https://www.amazon.eg/gp/product/B081TNFJ2V/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1)

- [Lithium Ion Battary charger](https://www.amazon.eg/gp/product/B09XVH6QRJ/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1) to charge the batteries.

- [18650 Battery Case Holder 2 cells](https://www.amazon.eg/-/en/18650-Battery-Case-Holder-cells/dp/B0B6WQ4PYX/ref=sr_1_1?keywords=18650+battery+holder+2+cell&qid=1661802682&sprefix=2+18650+%2Caps%2C157&sr=8-1)

- [Robustline Hot Glue stick](https://www.amazon.eg/-/en/Sticks-24PCS-Clear-Craft-Decoration/dp/B099QK9DRP/ref=sr_1_4?crid=383LVQ81Q9H95&keywords=Hot+Glue&qid=1661802883&sprefix=rhot+glue%2Caps%2C185&sr=8-4).

- [Tin Roll](https://www.amazon.eg/-/en/0-6-Oz-Gram-Tin-Roll/dp/B09813SFZ7/ref=sr_1_3?keywords=tin+roll&qid=1661801185&sprefix=Tin+%2Caps%2C144&sr=8-3) for soldering.

- [40 watt Soldering Iron](https://www.amazon.eg/-/en/Electric-Ceramic-Heating-Element-Soldering/dp/B08YNSYKW8/ref=sr_1_6?crid=Y0Z0D3O61P5M&keywords=40+watt+soldering+iron&qid=1661801933&sprefix=40+watt+soldering+iron%2Caps%2C139&sr=8-6)

- [Digital MutiMeter](https://www.amazon.eg/-/en/Generic-Digital-Multimeter-DT9205A/dp/B08YNCV5B1/ref=sr_1_7?keywords=digital+multimeter&qid=1661801259&sprefix=digital+mul%2Caps%2C149&sr=8-7) for testing the connections and measuring the volt.

- [Female to female jumper wires](https://www.amazon.eg/-/en/40PCS-Jumper-2-54mm-Female-Arduino/dp/B0922WNT57/ref=sr_1_3?crid=3NBN5U1MXA3Q2&keywords=female+to+female+jumper+wire&qid=1661801439&sprefix=female+to+female+jumper+wire%2Caps%2C148&sr=8-3)

- [Female to male jumper wire](https://www.amazon.eg/-/en/Arduino-Female-Male-Jumper-Wire/dp/B07NDZNLCF/ref=sr_1_2?keywords=female+to+male+jumper+wire&qid=1661801352&sprefix=Female+to+male+jum%2Caps%2C140&sr=8-2)

- [3A Power Bank](https://www.amazon.eg/gp/product/B08SFHT1VV/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1)

- [3A Mobile Charger with usb-c to usb-c cable](hhttps://www.amazon.eg/-/en/Type-C-Charger-Compatible-Samsung-Y-Mobile/dp/B0B652XD99/ref=sr_1_13?crid=360QVZ3SCUSVB&keywords=3A+Mobile+charger&qid=1661801555&sprefix=3a+mobile+charger%2Caps%2C147&sr=8-13) to charge the power bank and connect the powerbank to the board.

- [Wifi Router](https://www.amazon.eg/-/en/TP-LINK-AC1900-Archer-C80-Gigabit/dp/B0859MHXXB/ref=sr_1_1?keywords=wifi+router&qid=1661801677&sprefix=Wifi+Router%2Caps%2C176&sr=8-1)

- [2 x Arduino Infrared speed Sensor Module LM393](https://www.amazon.eg/gp/product/B091J8KW64/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1) to measure the rpm of the wheels.

- [Wire Stripper & Cutter](https://www.amazon.eg/-/en/Wire-Stripper-Cutter-200mm-8/dp/B091DVNPV1/ref=d_pd_sbs_sccl_2_2/261-6843811-0644055?pd_rd_w=uIb5D&content-id=amzn1.sym.c144b15c-a30b-4945-b3c9-3e8bbff81b12&pf_rd_p=c144b15c-a30b-4945-b3c9-3e8bbff81b12&pf_rd_r=HSG2M4BFPZDSYH81WGRB&pd_rd_wg=Vf4hm&pd_rd_r=9555f66b-c9c7-46c2-b84f-fadbfe3c983c&pd_rd_i=B091DVNPV1&psc=1)

- [small knife](https://www.amazon.eg/-/en/Knife-stainless-steel-multi-color/dp/B09WYWPPKK/ref=sr_1_18?keywords=knife&qid=1661803046&sprefix=knif%2Caps%2C144&sr=8-18)

- [4 x Long cable ties](https://www.amazon.eg/-/en/0-19inch-Industrial-Resistant-Cable-Management/dp/B08TWDGGN6/ref=sr_1_2?crid=1BNBZRFBRZPDH&keywords=long+cable+ties&qid=1663920466&sprefix=long+cable+tie%2Caps%2C144&sr=8-2)

- [A Pack of short cable ties](https://www.amazon.eg/-/en/100-Nylon-Cable-ties-150mm/dp/B0976H214T/ref=sr_1_3?crid=1BNBZRFBRZPDH&keywords=long+cable+ties&qid=1663920466&sprefix=long+cable+tie%2Caps%2C144&sr=8-3)

- 2 x (screw + nut)

## Steps

1. [Weld the motor wires](welding_the_motor_wires.md).

2. [Fixing the motors in the lower car floor](fixing_the_motors.md).

3. [Fixing the Lower Chassic Electronics](fix_lower_chassic_electronics.md)

4. [Fixing the Upper Chassic Electronics](fix_upper_chassic_electronics.md)

5. [Wiring](wiring.md)

6. [Fixing the Upper Chassic and the wheels](fixing_the_upper_chassic_and_the_wheels.md)


## Finished Robot

- When you want to switch on the robot:
  - Plug in the two charged 18650 batteries in the battery holder.
  - Connect Raspberri Pi to the power bank.

![Finished Robot](imgs/Final%20Robot.jpg)

## References

1. <a name="robotic_car_datasheet" href="https://media.digikey.com/pdf/Data%20Sheets/Seeed%20Technology/110090263_Web.pdf">Robotic Car Datasheet</a>.

2. <a name="assembly_video" href="https://www.youtube.com/watch?v=kewza7RyKMQ">Illustrative Robot Assembly Video</a>.
