# Fixing The Lower Chassic Electronics

## Description

We will fix electronics in the lower side of the car chassic.

## Tools

- Power bank

- H-Bridge IC

- 2 x Long cable ties

- 2 x Short cable ties

## Steps

- Fix the power bank at the front of the chassic using two long cable ties.

![fixing the power bank](imgs/Robot_Front.jpg)

- Make sure that there is a clearance for the power bank USB-C cable.

![power bank clearance](imgs/fix_powerbank_clearance.jpg)

- Fix the H-Bridge IC at the back of the chassic using two short cable ties.

![H-Bridge IC upper side](imgs/H_bridge_upperside.jpg)

![H-Bridge IC lower side](imgs/H_bridge_lowerside.jpg)
