# Robot Wiring

## Description

In this step we will connect all the electronics with each other.

## Tools

- Many Male to Female Wires.

- Many Female to Female Wires.

- Soldering Iron.

- Tin Solder.

- Wire Stripper and Cutter.

## Steps

- Connect the three jumpers of the H-Bridge to their places, we will not control the motors speed and we will enable the onboard regulator.

- Connect the Camera Cable to a usb3 Port in the pi board.

- See the following schematic of the H-Bridge Sensor.

![L298 Schematic](imgs/L298N-Schematic.jpg)

- See the following pin configuration of the PI Board.

![Pi pin configuration](imgs/PI%20Board%20GPIO.png)

- See the follwing schematic of speed sensor, not that the pin configuration will be reversed because we glued the sensors on their backs.

![Speed Sensor Schematic](imgs/speed_sensor_top_view.jpg)

- See the follwing image to know, what are the motors & sensors labels.

![Labels](imgs/labels.jpg)

![Labels Robot Back](imgs/labels_2.jpg)

- Connect the pins as shown in the following circuit digram, you can click on the image to enlarge it.

[![Curcuit Diagram](imgs/Circuit%20Diagram.png)](imgs/Circuit%20Diagram.png)

## Notes

- Motors 1 & 2 are connected in parallel.

- Motors 3 & 4 are connected in parallel.

- Speed Sensor 1 measures Motor 2 RPM.

- Speed Sensor 2 measures Motor 4 RPM.

- RPM of Motors 1 & 2 are assumed to be the same because they are connected in parallel.

- RPM of Motors 3 & 4 are assumed to be the same because they are connected in parallel.

- Speed Sensor 1 is powered from the LN298 Chip, we could have used the pi board as sensor 2.

- Speed Sensor 2 is powered from the Pi Board, we could have used the LN298 Chip as sensor 1.

- Pi board pins 8, 10, 12, 16 are used to control the motors motion.

- Pi board Pin 6 is used to connect Pi board ground to the batteries ground.

- Pi board Pins 2 & 20 are used to power sensor 2.

- Pi board Pins 18 & 22 are used to read the speed sensors reading.

## References

- [Raspeberri Pi 4 Electrical Diagram](raspberry-pi-4-reduced-schematics.pdf)

- [LN298 Dual H-bridge Datasheet](L298_H_Bridge.pdf)