# Welding The Robot Motor Wires

## Description

In this step we will weld the wires in the 4 motors.

## Tools

- Soldering Iron.
- Tin Solder (For soldering).
- Black wire (Ground Wire).
- Red wire (Positive voltage Wire).
- 1 x 18650 Battery (To test connectivity after soldering).
- Battery Charger.
- Wire Stripper.

## Steps
- Charge the battery using the battery charger.
- For Each motor do the following:
  - Strip one black wire (Ground wire) from both sides.
  - Strip one red wire (Positive wire) from both sides.
  - Solder the two wires accoding to [Motors Datasheet](#motors_datasheet), becarefull with the polarity.
  - Check the rotation direction using the charged battery with the previously soldered motors.

## Final Motors after soldering
- Here are the 4 motors after soldering
    
![Motors After Weding](imgs/Motors_After_Welding.jpg)

## References
1. <a name="motors_datasheet" href="https://www.adafruit.com/product/3777">TT Motors Datasheet</a>.