# System Preparation

## Description
In this section we will install the os, ROS and other system libraries.

## Hardware Components
- [Micro SD Card Reader](https://www.amazon.eg/-/en/Micro-SD-T17-Card-Reader/dp/B091FPB9WK/ref=sr_1_9?keywords=card+reader&qid=1664134773&sprefix=card+%2Caps%2C138&sr=8-9)
- [Micro 32GB SD Card](https://www.amazon.eg/-/en/Kingston-32GB-MicroSD-Canvas-Adaptor/dp/B07YGZ7FY7/ref=sr_1_1?keywords=32+gb+sd+card&qid=1664134834&sprefix=32+GB+%2Caps%2C137&sr=8-1)
- Laptop.
- Wifi Router.

# Steps
1. [Install Rapspberry Pi OS](Install_PI_OS.md).
2. [Install ROS2 foxy](Install-ROS2-Foxy.md).
3. [Install the ROS Nodes for the cameras](Install-the-two-Cameras.md).
4. [Install the Pi GPIO Library](Install-PI-GIPO-Library.md).
