# Install ROS2 Foxy

## Description
In this step we will install ROS2 Foxy on the PI & host computer

## Steps
- Install ROS2 Foxy on the host computer using the [installation page](https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html), install `ros-foxy-desktop`
- On the pi (using ssh) follow the  same steps inthe [installation page](https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html), this time install ros-foxy-ros-base
- Add sourcing of ros to .bashrc
```
echo "source /opt/ros/foxy/setup.bash" >> ~/.bashrc
```
- Install demo nodes cpp package
```
sudo apt install ros-foxy-demo-nodes-cpp -y
```

- In a first terminal instaniate a talker node (on pi)
```
ros2 run demo_nodes_cpp talker
```
- In another terminal instaniate the listner node (on pi)
```
ros2 run demo_nodes_cpp listener
```
- In the host computer which will be on the same network you should be able to see the nodes
```
ros2 node list
```
- In the host computer you should be able to listen to the chatter
```
ros2 topic echo /chatter
```
