# Install the Pi GPIO Library

## Description
In this step we will install the PI GPIO Library

## Steps
- Open ssh connection to pi.
- Install python3-pip
```
sudo apt install python-pip3 -y
```
- Install the pi python GPIO Library
```
sudo pip install RPi.GPIO
```
