# Install Raspberri Pi OS

## Description
In this step we will flash the Rapberri Pi OS using a card reader.

## Tools
- USB Card Reader.
- Laptop with usb port.
- A fast Internet Connection.
- 32 GB micro sdcard.

## Steps
- Install Raspberri pi imager using: https://www.raspberrypi.com/software/
- Mount the sdcard using sdcard reader
- Choose Storage as the sdcard.
- Choose OS
  - Choose  Other Raspberri pi images. 
  - Choose Ubuntu Server 20.04.4 LTS 64-bit.
- Set these settings before writting.
  - Set Image customization Options to: to always use.
  - enable ssh
  - set the username and password.
  - Configure wireless lan.
  - Configure Timezone and Keyboard layout.
  - If you are using hidden network select the corresponding option.
  - Set the writting to play sound after finishing.
  - Click Save.
- Click write.
- Wait until the it finishes and play the sound.
- Umount the sdcard reader.
- Mount the card into Rapberri pi.
- Power on the board.
- Wait for the board to connect to wifi (wait about 15 minutes - only for the first time).
- Check that you can access it using ssh.
- Update the system
```
sudo apt update
sudo apt dist-upgrade
```