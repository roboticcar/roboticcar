# Open Source Robotic Car Using Rasberry PI 4 & ROS 2

## Description

- This is a Robotic Car Project Using Raspberry pi 4 and ROS2 foxy.
- You Can control this Car from your Computer using the Wifi and your Keyboard.

![Finished Robot](imgs/Final%20Robot.jpg)

## Demo Video

[![Demo Video](https://img.youtube.com/vi/hNlN-XCIJQo/0.jpg)](https://www.youtube.com/watch?v=hNlN-XCIJQo)

## Steps

1. [Construct the Robot](Construction/README.md).
2. [Install the OS and depenencies](System%20Preparation/README.md).
3. [Clone the repo and run the ROS Nodes](ROS%20Nodes/README.md). 

## Status

- This project is done.