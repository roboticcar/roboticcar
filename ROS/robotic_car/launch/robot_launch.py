# Author: Remon Kamal
# Date: 26-09-2022

import os
import pathlib
from pickle import TRUE
import launch
from launch_ros.actions import Node
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.conditions import LaunchConfigurationEquals
from launch.conditions import LaunchConfigurationNotEquals
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import ComposableNodeContainer
from launch_ros.actions import LoadComposableNodes
from launch_ros.descriptions import ComposableNode


package_name = 'robotic_car'


def generate_launch_description() -> LaunchConfiguration:
    """Returns the launch configuration to be launched by the file.

    Returns:
        LaunchConfiguration: The ROS nodes and launch configuration to be
            launched.
    """
    # The Robot driver node that drives the motors
    my_robot_driver = Node(
        package=package_name,
        executable='my_robot_driver',
        output='screen',
    )

    # The sensor node that launches the reading of the RPM Sensors.
    sensor_node = Node(
        package=package_name,
        executable='sensor_node',
        output='screen',
    )

    # The Left Camera node, that reades left camera images.
    left_camera = Node(
        package='v4l2_camera',
        executable='v4l2_camera_node',
        output='screen',
        remappings=[
            ('image_raw', 'left_image_raw')
        ],
        parameters=[{
                'video_device': "/dev/video0",
                # Decreasing the image size to avoid streaming high bandwidth
                'image_size': [100, 100],
            }],
    )

    # The Right Camera node that reads camera images from the right camera
    right_camera = Node(
        package='v4l2_camera',
        executable='v4l2_camera_node',
        output='screen',
        remappings=[
            ('image_raw', 'right_image_raw')
        ],
        parameters=[{
                'video_device': "/dev/video2",
                # Decreasing the image size to avoid streaming high bandwidth
                'image_size': [100, 100],
            }],
    )

    # Return the final launch configuration that launches all the nodes
    return LaunchDescription([
        my_robot_driver,
        sensor_node,
        left_camera,
        right_camera,
    ])
