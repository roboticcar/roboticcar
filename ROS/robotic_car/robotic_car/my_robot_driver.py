# Author: Remon Kamal
# Date: 26-09-2022

import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile
from geometry_msgs.msg import Twist

import RPi.GPIO as GPIO


class RobotDriver(Node):
    def __init__(self):
        """This is the main Robotic Car Driver that drives the motors and
        steers the car.
        """
        super().__init__('robot_driver')

        self.get_logger().info('robot_driver online...')
        qos_profile = QoSProfile(depth=10)
        # Those are the H-bridge inputs from input1 to input 4
        # Those are hadware input pins of the PI board
        self.h_bridge_input_pins_ = [16, 12, 10, 8]
        # A Dictionary that Maps Motion to H-Bridge inputs
        self.motion_to_inputs_ = {
            'FORWARD': [GPIO.HIGH, GPIO.LOW, GPIO.HIGH, GPIO.LOW],
            'BACKWARD': [GPIO.LOW, GPIO.HIGH, GPIO.LOW, GPIO.HIGH],
            'LEFT': [GPIO.LOW, GPIO.HIGH, GPIO.HIGH, GPIO.LOW],
            'RIGHT': [GPIO.HIGH, GPIO.LOW, GPIO.LOW, GPIO.HIGH],
            'STOP': [GPIO.LOW, GPIO.LOW, GPIO.LOW, GPIO.LOW],
        }
        # Initializes the board pins
        self.initialize_board()
        # Subscribe to the velocity commands for constrol actions
        self.vel_subscription = self.create_subscription(
            Twist, '/cmd_vel', self.vel_listener_callback, qos_profile)

    def vel_listener_callback(self, twist_msg: Twist) -> None:
        """The Velocity topic callback, were it applies the velocity to the
        motors
        **note**:
            - It applies only the direction of the velocity, but the robot
              always uses the max velocity during its motion.
            - It can only do one motion (forward, backward, left, right) it
              cannot do a mixture of motions like going forward with a left
              turn.

        Args:
            twist_msg (Twist): The velocity messages to listen to.
        """
        # Go forward if the linear x component is positive
        if twist_msg.linear.x > 0:
            self.get_logger().info('Gowing Forward')
            self.move_robot('FORWARD')
        # Go backward if the linear x component is negative
        elif twist_msg.linear.x < 0:
            self.get_logger().info('Gowing Backward')
            self.move_robot('BACKWARD')
        # Turn left if the rotation about z-axis is +ve
        elif twist_msg.angular.z > 0:
            self.get_logger().info('Turning Left')
            self.move_robot('LEFT')
        # Turn right if the rotation about z-axis is -ve
        elif twist_msg.angular.z < 0:
            self.get_logger().info('Turning Right')
            self.move_robot('RIGHT')
        # Otherwise stop the robot.
        else:
            self.get_logger().info('Stoping')
            self.move_robot('STOP')

    def initialize_board(self) -> None:
        """Initializes the board pins mode for additional control after that.
        """
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.h_bridge_input_pins_, GPIO.OUT)

    def move_robot(self, motion: str) -> None:
        """Applies a certain move to the motors of the robot

        Args:
            motion (str): The move tag.
        """
        GPIO.output(self.h_bridge_input_pins_, self.motion_to_inputs_[motion])

    def destroy_node(self):
        """A Node desctructor that ensures the releasing of the RPI library
        resources when it exits.
        """
        self.get_logger().info('Cleaning GPIO Resources')
        GPIO.cleanup()
        super().destroy_node()


def main() -> None:
    """Main function of the node.
    """
    # Initializing ROS
    rclpy.init(args=None)
    # Creating the node.
    robot_driver = RobotDriver()
    try:
        # Spinning the node.
        rclpy.spin(robot_driver)
    # Listening to a keyboard inetrrupt (Ctrl + C)
    except KeyboardInterrupt:
        pass
    # Cleaning the node resources
    robot_driver.destroy_node()
    # Shutting down ros
    rclpy.shutdown()


if __name__ == '__main__':
    main()
