# Author: Remon Kamal
# Date: 26-09-2022

import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile
from geometry_msgs.msg import Twist

import RPi.GPIO as GPIO


class SensorNode(Node):
    def __init__(self):
        """This is the sensor node that initialzies the rpm sensors and
        sends their transmission across the topics.
        """
        super().__init__('sensor_node')

        self.get_logger().info('sensor_node is online...')
        qos_profile_ = QoSProfile(depth=10)
        # Input sensor pins
        self.input_right_sensor_pin_ = 18
        self.input_left_sensor_pin_ = 22
        self.sensor_reporting_time_in_seconds_ = 1.0
        self.n_encoder_pitches_ = 20
        self.right_cycle_cnt_ = 0
        self.left_cycle_cnt_ = 0

        # Initializing the board
        self.initialize_board()
        # Creating the topics for publishing the sensor readings
        self.right_sensor_publisher_ = self.create_publisher(
            Twist, 'right_sensor', qos_profile_)
        self.left_sensor_publisher_ = self.create_publisher(
            Twist, 'left_sensor', qos_profile_)
        # Creating the timer that publishes the sensor messages
        self.timer = self.create_timer(
            self.sensor_reporting_time_in_seconds_, self.timer_callback)

    def count_right_cycles(self, channel: int) -> None:
        """A callback that will be called when the right sensor senses a
        falling edge on the sensor reading.

        Args:
            channel (int): A Dummy variable but this contains the channel
                number in which the falling edge has happened.
        """
        # Incrementing the cycles count
        self.right_cycle_cnt_ += 1

    def count_left_cycles(self, channel: int) -> None:
        """A callback that will be called when the left sensor senses a
        falling edge on the sensor reading.

        Args:
            channel (int): A Dummy variable but this contains the channel
                number in which the falling edge has happened.
        """
        # Incrementing the cycles count
        self.left_cycle_cnt_ += 1

    def timer_callback(self) -> None:
        """The timer callback that sends the rpm readings across the topics.
        """
        # Remeber the cycle cnts
        tmp_right_cycles_cnt = self.right_cycle_cnt_
        tmp_left_cycles_cnt = self.left_cycle_cnt_
        # zeroing the cycle cnt
        self.right_cycle_cnt_ = 0
        self.left_cycle_cnt_ = 0
        # Create the sensor messages
        left_sensor_msg = Twist()
        right_sensor_msg = Twist()
        # Calculate and set the sensor messages
        left_sensor_msg.angular.y = tmp_left_cycles_cnt / \
            (self.n_encoder_pitches_ * self.sensor_reporting_time_in_seconds_)
        right_sensor_msg.angular.y = tmp_right_cycles_cnt / \
            (self.n_encoder_pitches_ * self.sensor_reporting_time_in_seconds_)
        # Publish the sensor messages
        self.left_sensor_publisher_.publish(left_sensor_msg)
        self.right_sensor_publisher_.publish(right_sensor_msg)

    def initialize_board(self) -> None:
        # Marking pin mode configuration as the board pins
        GPIO.setmode(GPIO.BOARD)
        # Setting sensor inputs as inputs
        GPIO.setup(
            [self.input_right_sensor_pin_, self.input_left_sensor_pin_],
            GPIO.IN)
        # Putting the edge counting callback
        GPIO.add_event_detect(
            self.input_right_sensor_pin_, GPIO.FALLING,
            callback=self.count_right_cycles)
        GPIO.add_event_detect(
            self.input_left_sensor_pin_,
            GPIO.FALLING, callback=self.count_left_cycles)

    def destroy_node(self):
        """A Node desctructor that ensures the releasing of the RPI library
        resources when it exits.
        """
        self.get_logger().info('Cleaning GPIO Resources')
        GPIO.cleanup()
        super().destroy_node()


def main() -> None:
    """Main function of the node.
    """
    # Initializing ROS
    rclpy.init(args=None)
    # Creating the node.
    sensor_node = SensorNode()
    try:
        # Spinning the node.
        rclpy.spin(sensor_node)
    # Listening to a keyboard inetrrupt (Ctrl + C)
    except KeyboardInterrupt:
        pass
    # Cleaning the node resources
    sensor_node.destroy_node()
    # Shutting down ros
    rclpy.shutdown()


if __name__ == '__main__':
    main()
